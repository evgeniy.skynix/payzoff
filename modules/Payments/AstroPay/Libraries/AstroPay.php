<?php

namespace Payments\AstroPay\Libraries;

use Payments\PaymentsBase;

/**
 * Class of AstroPay
 *
 */
class AstroPay extends PaymentsBase
{
    const PAYMENT_NAME = 'AstroPay';

    private static $x_login;
    private static $x_trans_key;
    private static $x_login_ws;
    private static $x_trans_key_ws;
    private static $secret_key;
    private static $instance = null;

    private static $sandbox = true;



    private static $url = array(
        'newinvoice' => '',
        'status' => '',
        'exchange' => '',
        'banks' => ''
    );

    private static $errors = 0;

    public static function getInstance()
    {
        if (null === self::$instance)
        {
            self::$instance = new self();
        }
        return self::$instance;
    }

    private  function __construct()
    {
        self::$errors = 0;

        $key = self::$sandbox ? 'astropay.dev.' : 'astropay.';

        self::$x_login           = getenv($key . 'x_login');
        self::$x_trans_key       = getenv($key . 'x_trans_key');
        self::$secret_key        = getenv($key . 'secret_key');
        self::$x_login_ws        = getenv($key . 'x_login_for_webpaystatus');
        self::$x_trans_key_ws    = getenv($key . 'x_trans_key_for_webpaystatus');
        self::$url['newinvoice'] = getenv($key . 'newinvoice');
        self::$url['status']     = getenv($key . 'status');
        self::$url['exchange']   = getenv($key . 'exchange');
        self::$url['banks']      = getenv($key . 'banks');
    }

    private function __clone() {}

    private function __wakeup() {}

    /**
     * Method create new Invoice
     *
     * @param $invoice
     * @param $amount
     * @param $bank
     * @param $country
     * @param $iduser
     * @param $cpf
     * @param $name
     * @param $email
     * @param string $currency
     * @param string $description
     * @param string $bdate
     * @param string $address
     * @param string $zip
     * @param string $city
     * @param string $state
     * @param string $return_url
     * @param string $confirmation_url
     * @return mixed
     */
    public static function newInvoice(
        $invoice,
        $amount,
        $bank,
        $country,
        $iduser,
        $cpf,
        $name,
        $email,
        $currency = null,
        $description = null,
        $bdate = null,
        $address = null,
        $zip = null,
        $city = null,
        $state = null,
        $return_url = null,
        $confirmation_url = null
    )
    {
        $paramsArray = array(

            'x_login' => self::$x_login,
            'x_trans_key' => self::$x_trans_key,
            'x_invoice' => $invoice,
            'x_amount' => $amount,
            'x_bank' => $bank,
            'type' => 'json',
            'x_country' => $country,
            'x_iduser' => $iduser,
            'x_cpf' => $cpf,
            'x_name' => $name,
            'x_email' => $email,
        );

        //Optional
        if (!empty($currency)) {
            $paramsArray['x_currency'] = $currency;
        }
        if (!empty($description)) {
            $paramsArray['x_description'] = $description;
        }
        if (!empty($bdate)) {
            $paramsArray['x_bdate'] = $bdate;
        }
        if (!empty($address)) {
            $paramsArray['x_address'] = $address;
        }
        if (!empty($zip)) {
            $paramsArray['x_zip'] = $zip;
        }
        if (!empty($city)) {
            $paramsArray['x_city'] = $city;
        }
        if (!empty($state)) {
            $paramsArray['x_state'] = $state;
        }
        if (!empty($return_url)) {
            $paramsArray['x_return'] =  ''; //'http://zoff.000webhostapp.com/answer'; //$return_url;
        }
        if (!empty($confirmation_url)) {
            $paramsArray['x_confirmation'] = $confirmation_url;
        }

        $paramsArray['control'] = self::getControl($invoice, $amount, $iduser, $bank, $cpf, $bdate, $email, $zip, $address, $city, $state);

        $response = self::curl(self::$url['newinvoice'], $paramsArray);
        return $response;
    }

    /**
     * Return control
     *
     * @param $invoice
     * @param $amount
     * @param $iduser
     * @param $bank
     * @param $cpf
     * @param $bdate
     * @param $email
     * @param $zip
     * @param $address
     * @param $city
     * @param $state
     * @return string
     */
    public static function getControl($invoice, $amount, $iduser, $bank, $cpf, $bdate, $email, $zip, $address, $city, $state)
    {
        $message = $invoice . 'V' . $amount . 'I' . $iduser . '2' . $bank . '1' . $cpf . 'H' . $bdate . 'G' . $email . 'Y' . $zip . 'A' . $address . 'P' . $city . 'S' . $state . 'P';
        $control = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', self::$secret_key)));

        return $control;
    }

    /**
     * @param $invoice
     * @return mixed
     */
    public static function getStatusInvoice($invoice)
    {
        $paramsArray = array(

            'x_login' => self::$x_login_ws,
            'x_trans_key' => self::$x_trans_key_ws,
            'x_invoice' => $invoice,
            'type'         => 'json'
        );

        $response = self::curl(self::$url['status'], $paramsArray);

        return $response;
    }

    /**
     * @param string $country
     * @param int $amount
     * @return mixed
     */
    public static function getExchange($country = 'BR', $amount = 1)
    {
        $paramsArray = array(
            'x_login'     => self::$x_login_ws,
            'x_trans_key' => self::$x_trans_key_ws,
            'x_country'   => $country,
            'x_amount'    => $amount,
            'type'         => 'json'
        );

        $response = self::curl(self::$url['exchange'], $paramsArray);
        return $response;
    }

    /**
     * @param string $country
     * @param string $type
     * @return mixed
     */
    public static function getBanksByCountry($country = 'BR', $type = 'json')
    {
        $paramsArray = array(
            'x_login'      => self::$x_login,
            'x_trans_key'  => self::$x_trans_key,
            'country_code' => $country,
            'type'         => $type
        );

        $response = self::curl(self::$url['banks'], $paramsArray);
        return $response;
    }

}
