<?php

namespace Payments\AstroPay\Models;

class Codes
{
    const SALE_RESULT_CODES = [
        1 => 'Invoice already used',
        6 => 'Invalid transaction',
        7 => 'Transaction pending',
        8 => 'Transaction rejected (final status)',
        9 => 'Amount paid. Transaction successfully concluded (final status)'
    ];

    const ERROR_CODES = [
        300 => 'Invalid params + [param name]',
        301 => 'Empty params + [param name]',
        302 => 'Invalid control string',
        303 => 'Invalid request',
        401 => 'Invalid credentials',
        402 => 'Unregistered IP address',
        403 => 'Merchant has no authorization to use this API',
        500 => 'Invoice already used',
        501 => 'The user must be adult',
        502 => 'User unauthorized',
        504 => 'User unauthorized due to cadastral situation',
        506 => 'Payment method not found',
        507 => 'Country not supported',
        508 => 'User limit exceeded',
        510 => 'Invalid transaction status',
        511 => 'Amount exceeded',
        512 => 'Email associated with another country',
        601 => 'Currency not allowed for this country',
        603 => 'Token not found or inactive',
        604 => 'Credit card not found',
        605 => 'Mismatch credit card – user data',
        606 => 'Transaction not found',
        704 => 'Could not process transaction',
        705 => 'Could not communicate with acquirer',
        800 => 'Rejected by bank',
        801 => 'Pending for contingency',
        802 => 'Rejected due to card blacklisted',
        803 => 'Rejected due insufficient amount',
        804 => 'Rejected due bad + [param name]',
        805 => 'Rejected due max attempts reached',
        806 => 'User must call bank for authorize',
        807 => 'Rejected due duplicated payment',
        808 => 'Rejected due credit card disabled',
        809 => 'Rejected due score validation',
    ];

    const REFUND_CODES = [
        0 => 'Refund / chargeback pending',
        1 => 'Amount refunded/ chargeback successfully (final status)',
        2 => 'Refund rejected or failed',
        3 => 'Refund/chargeback cancelled / reimbursed (final status)',
    ];

    const PAYMENT_CODES = [
        'VI' => ['name' => 'Visa', 'counrty' => ['BR', 'MX', 'CO']],
        'MC' => ['name' => 'Mastercard', 'counrty' => ['BR', 'MX', 'CO', 'AR']],
        'EL' => ['name' => 'Elo', 'counrty' => ['BR']],
        'HI' => ['name' => 'Hipercard', 'counrty' => ['BR']],
        'DC' => ['name' => 'Diners', 'counrty' => ['BR', 'CO']],
        'AE' => ['name' => 'American express', 'counrty' => ['BR', 'CO', 'AR']],
        'ML' => ['name' => 'Cartao', 'counrty MercadLivre' => ['BR']],
        'VD' => ['name' => 'Visa', 'counrty debit' => ['MX']],
        'MD' => ['name' => 'Mastercard', 'counrty Debit' => ['MX']],
        'NJ' => ['name' => 'Naranja', 'counrty' => ['AR']],
        'NT' => ['name' => 'Nativa', 'counrty Master Card' => ['AR']],
        'TS' => ['name' => 'Tarjeta', 'counrty Shopping' => ['AR']],
        'CS' => ['name' => 'Cencosud', 'counrty' => ['AR']],
        'CL' => ['name' => 'Cabal', 'counrty' => ['AR']],
        'AG' => ['name' => 'Argencard', 'counrty' => ['AR']],
    ];

    const BANK_CODES = [
        163 => ['name' => 'Banorte', 'payment' => ['VI', 'MC']],
        164 => ['name' => 'HSBC', 'payment' => ['VI', 'MC', 'VD', 'MD']],
        165 => ['name' => 'Scotiabank', 'payment' => ['VI', 'MC']],
        160 => ['name' => 'Santander', 'payment' => ['VI', 'MC', 'VD', 'MD']],
        1023 => ['name' => 'Inbursa', 'payment' => ['VI', 'MC']],
        1020 => ['name' => 'Ixe', 'payment' => ['VI', 'MC']],
        1021 => ['name' => 'Bajio', 'payment' => ['VI', 'MC']],
        1024 => ['name' => 'Mifel', 'payment' => ['VI', 'MC']],
        1018 => ['name' => 'Banco Ahorro Famsa', 'payment' => ['VI', 'MC']],
        1022 => ['name' => 'Banregio', 'payment' => ['VI', 'MC']],
        1019 => ['name' => 'Invex', 'payment' => ['VI', 'MC']],
        1017 => ['name' => 'Afirme', 'payment' => ['VI', 'MC']],
        158 => ['name' => 'Bancomer', 'payment' => ['VI', 'MC', 'VD']],
        159 => ['name' => 'Banamex', 'payment' => ['VI', 'MC', 'VD', 'MD']],
        162 => ['name' => 'Other', 'payment' => ['VI', 'MC']],
    ];

    const COUNTRY_CODES = [
        "AR" => "Argentina",
        "BR" => "Brazil",
        "CL" => "Chile",
        "CN" => "China",
        "CO" => "Colombia",
        "MX" => "Mexico",
        "PY" => "Paraguay",
        "PE" => "Perú",
        "UY" => "Uruguay",
    ];
}