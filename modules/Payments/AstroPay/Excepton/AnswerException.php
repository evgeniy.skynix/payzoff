<?php
namespace Payments\AstroPay\Exception;

use Throwable;

class AstroPayAnswerException extends \Exception
{
    //@TODO For production: Custom handle answer from payment system
    public function __construct(string $message = "", int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

    }
}