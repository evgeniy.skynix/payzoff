<?php

namespace Payments\AstroPay\Config;

use Config\Validation as MainValidation;

class Validation extends MainValidation
{
    public $billing = [
        'username'  => 'required',
        'firstName' => 'required',
        'lastName'  => 'required',
        'x_cpf'     => 'required',
        'country'   => 'required',
        'amount'    => 'required',
        'email'     => 'required|valid_email'
    ];

    public $billing_errors = [
        'username' => [
            'required'    => 'You must choose a username.',
        ],
        'email'    => [
            'valid_email' => 'Please check the Email field. It does not appear to be valid.'
        ]
    ];
}
