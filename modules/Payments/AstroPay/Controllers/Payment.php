<?php

namespace Payments\AstroPay\Controllers;


use Payments\AstroPay\Models\Codes;
use Payments\AstroPay\Config\Validation;
use CodeIgniter\Controller;
use Payments\AstroPay\Libraries\AstroPay;
use Payments\PaymentsInterface;
use Payments\AstroPay\Exception\AstroPayAnswerException;

class Payment extends Controller implements PaymentsInterface
{
    const PAYMENT_NAME = 'AstroPay';

    const VIEW_NS = 'Payments\AstroPay\Views';

    private $data = [
        'css' => '',
        'h1' => '',
        'bt' => 0,
        'countries' => [],
        'error' => null,
        'post' => [],
    ];

    /**
     * Render main page
     */
    public function index()
    {
        $tpl = self::VIEW_NS . '\index';
        return $this->getRender($tpl, $this->data);
    }

    /**
     * Render example billing form
     *
     * @param int $v
     * @return string
     */
    public function billing(int $v = 1)
    {
        $post = [];
        $error = null;
        if ($this->isLocal()) {
            $body = $this->request->getBody();
            parse_str($body, $post);

            $error = $this->checkout();
        }

        $data = [
            'css' => 'form-validation.css',
            'h1' => 'Billing form. Example ' . $v,
            'bt' => $v,
            'countries' => Codes::COUNTRY_CODES,
            'error' => $error,
            'post' => $post,
        ];

        $data = array_merge($this->data, $data);

        $tpl = self::VIEW_NS . '\billing' . $v;
        $js = self::VIEW_NS . '\validateJS';
        return $this->getRender($tpl, $data, $js);
    }

    /**
     * Create new invoice
     *
     * @return array
     * @throws AstroPayAnswerException
     */
    public function checkout()
    {
        $post = $_POST;
        $body = $this->request->getBody();
        parse_str($body, $params);

        if (!$this->validate((new Validation())->billing)) {
            return [
                'description' => 'Some require field is empty',
                'param' => '',
            ];
        }

        $invoice     = $this->getNewInvoiceId();
        $fullName    = sprintf("%s %s", $params['firstName'], $params['lastName']);
        $amount      = $params['amount'];
        $iduser      = $params['username'];
        $cpf         = $params['x_cpf'];
        $country     = $params['country'];
        $email       = $params['email'];
        $bank        = $params['bank'] ?? 'VI';
        $bdate       = $params['bdate'] ?? null;
        $zip         = $params['zip'] ?? null;
        $address     = $params['address'] ?? null;
        $city        = $params['city'] ?? null;
        $state       = $params['state'] ?? null;
        $currency    = $params['currency'] ?? null;
        $description = $params['description'] ?? null;

        //@TODO for production: Save invoiceControl
        $invoiceControl = AstroPay::getInstance()::getControl(
            $invoice,
            $amount,
            $iduser,
            $bank,
            $cpf,
            $bdate,
            $email,
            $zip,
            $address,
            $city,
            $state
        );

        $response = AstroPay::getInstance()::newInvoice(
            $invoice,
            $amount,
            $bank,
            $country,
            $iduser,
            $cpf,
            $fullName,
            $email,
            $currency,
            $description,
            $bdate,
            $address,
            $zip,
            $city,
            $state
        );
        $decoded_response = json_decode($response);

        if ($decoded_response->status == 0) {
            $url = $decoded_response->link;
            header("Location: $url");
            die();
        } else {
            $_POST = $post;
            return $this->errorHandler($decoded_response);
        }
    }

    /**
     * Handle of errors
     *
     * @param $decoded_response
     * @return array
     * @throws AstroPayAnswerException
     */
    private function errorHandler($decoded_response): array
    {
        //@TODO If InvoiceID already exist, generate new. ONLY TESTING!!!!!
        if ($decoded_response->error_code == 500) {
            $this->checkout();
        }

        if (in_array($decoded_response->error_code, [300, 301])) {
            $desc = explode(' ', $decoded_response->desc);
            $param = array_pop($desc);
            $desc = implode(' ', $desc);
            $error = [
                'description' => $desc,
                'param' => $param,
            ];
        } else {
            $error = Codes::ERROR_CODES[$decoded_response->error_code];
            throw new AstroPayAnswerException($error);
        }

        return $error;
    }

    /**
     * Method get answer from payment system
     */
    public function answer(): string
    {
        $body = $this->request->getBody();
        parse_str($body, $params);

        //@TODO for production: test 'invoiceControl' with 'answerControl'
        //$invoiceControl == $params['x_control'];

        $invoice = $params['x_invoice'];

        $error = null;
        $response['result'] = Codes::SALE_RESULT_CODES[$params['result']];

        $response['Invoice'] = $params['x_invoice'];
        $response['Username'] = $params['x_iduser'];
        $response['Document'] = $params['x_document'];

        $response['Amount'] = $params['x_amount'];
        $response['Amount_usd'] = $params['x_amount_usd'];
        if (!empty($params['x_description'])) {
            $response['Description'] = $params['x_description'];
        }

        if (!empty($params['parent_invoice'])) {
            $response['parent_invoice'] = $params['parent_invoice'];
        }

        $data = [
            'css' => 'grid.css',
            'h1' => 'Invoice data.',
            'bt' => 3,
            'error' => $error,
            'invoice' => $invoice,
            'rows' => $response
        ];
        $js = self::VIEW_NS . '\validateJS';
        $tpl = self::VIEW_NS . '\answer';
        $data = array_merge($this->data, $data);

        return $this->getRender($tpl, $data, $js);
    }


    /**
     * Render list Banks of country. Default country BR
     */
    public function getBanks(): string
    {
        $banks = AstroPay::getInstance()::getBanksByCountry('BR');
        $banks = json_decode($banks, 1);

        $data = [
            'css' => 'album.css',
            'h1' => 'List of Banks working with AstroPay on Brazil',
            'bt' => 4,
            'banks' => $banks,
        ];
        $tpl = self::VIEW_NS . '\bunks';
        $data = array_merge($this->data, $data);
        return $this->getRender($tpl, $data);
    }

    /**
     * Render status of invoice
     *
     * @param null $invoice
     * @return string
     */
    public function getPayStatus($invoice = null): string
    {

        $response = [];
        $error = null;

        if ($this->isLocal()) {
            $body = $this->request->getBody();
            parse_str($body, $params);
            $invoice = $params['invoiceID'] ?? null;
        }

        if ($invoice) {

            $response = AstroPay::getInstance()::getStatusInvoice($invoice);

            $response = json_decode($response, 1);
            $response['result'] = Codes::SALE_RESULT_CODES[$response['result']];
            unset($response['Sign']);
            if (-1 == $response['x_document']) {
                $error = 'Invoice not found';
                $response = [];
            }
        }

        $data = [
            'css' => 'grid.css',
            'h1' => 'Invoice data.',
            'bt' => 3,
            'error' => $error,
            'invoice' => $invoice,
            'rows' => $response
        ];
        $js = self::VIEW_NS . '\validateJS';
        $tpl = self::VIEW_NS . '\answer';
        $data = array_merge($this->data, $data);

        return $this->getRender($tpl, $data, $js);
    }

    /**
     *
     * @param $tpl
     * @param array $data
     * @param null $js
     * @return string
     */
    private function getRender($tpl, array $data = [], $js = null): string
    {
        $html = view(self::VIEW_NS . '\header', $data);
        $html .= view(self::VIEW_NS . '\menu', $data);
        $html .= view($tpl, $data);
        if ($js) {
            $html .= view(self::VIEW_NS . '\standardJS');
            $html .= view($js);
        }
        $html .= view(self::VIEW_NS . '\footer', $data);

        return $html;
    }

    /**
     * @return bool
     */
    private function isLocal(): bool
    {
        $host = parse_url($this->request->config->baseURL)['host'] && $this->request->getHeader('Host')->getValue();
        $method = 'post' == $this->request->getMethod();
        return $host && $method;
    }

    /**
     *
     * @return string
     */
    public function getExchange(): string
    {
        return AstroPay::getInstance()::getExchange('BR', 1);
    }

    /**
     * Generate random invoiceID.
     * @notice Only for testing.
     *
     * @return string
     */
    private function getNewInvoiceId(): string
    {
        list(, $usec) = explode(".", microtime(1));
        return 'Doc' . rand(0, 10000) . $usec;
    }

    /**
     * Mock method
     *
     * @TODO Implements on production version
     */
    public function confirm(): string
    {
        return '';
    }

    /**
     * Mock method
     *
     * @TODO Implements on production version
     */
    public function refund(): string
    {
        return '';
    }
}


