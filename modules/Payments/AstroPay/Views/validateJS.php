<script>
    (function () {
        'use strict';

        window.addEventListener('load', function () {

            if(document.getElementById('error')) {
                var id = document.getElementById('error').getAttribute('param');
                document.getElementById(id).classList.add('is-invalid');
            }
            var forms = document.getElementsByClassName('needs-validation');

            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
            /*
            if(var err = document.getElementById('error')) {
                alert(err.getAttribute('param'));
                //document.getElementById('x_cpf').classList.add('is-invalid');
            }
*/
        }, false);
    })();
</script>