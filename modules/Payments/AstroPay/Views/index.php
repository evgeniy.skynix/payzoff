<main role="main" class="container">
    <div class="starter-template">
        <h2>Example integrate <b>AstroPay</b> payment system.</h2>

        <p class="lead">
            <b>AstroPay</b> Card is a prepaid virtual card, accepted at hundreds of online stores integrated with AstroPay. Just register for free, choose the value of the card you want to purchase and pay in local currency using the most popular payment methods in your country.
        </p><p class="lead">
            It is an immediate solution, so you should not wait. Simply purchase your card, make your payment and receive an e-mail with your AstroPay Card details ready for use.
            Use this document as a way to quickly start any new project.</p>
    </div>

</main>
