<section class="jumbotron text-center">
    <div class="container">
        <p class="lead text-muted"></p>
        <p> <!-- btn-primary btn-secondary -->
            <a href="/" class="btn <?php echo $bt == 0 ? "btn-primary" : "btn-secondary"  ?> my-2">Main page</a>
            <a href="/billing/1" class="btn <?php echo $bt == 1 ? "btn-primary" : "btn-secondary"  ?> my-2">Example 1</a>
            <a href="/billing/2" class="btn <?php echo $bt == 2 ? "btn-primary" : "btn-secondary"  ?>  my-2">Example 2</a>
            <a href="/paystatus" class="btn <?php echo $bt == 3 ? "btn-primary" : "btn-secondary"  ?>  my-2">Check payment</a>
            <a href="/banks" class="btn <?php echo $bt == 4 ? "btn-primary" : "btn-secondary"  ?>  my-2">List of bank</a>
        </p>
        <h1 class="jumbotron-heading"><?=$h1?></h1>
    </div>
</section>