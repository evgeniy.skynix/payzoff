<main role="main">
    <div class="album py-5 bg-light">
        <div class="container">
            <div class="row">
                <?php foreach ($banks as $bank): ?>
                    <div class="col-md-4">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top" style="height:208px" data-src="<?= $bank['logo'] ?>"
                                 src="<?= $bank['logo'] ?>" alt="<?= $bank['name'] ?>">
                            <div class="card-body">
                                <span class="card-text">Bank: <?= $bank['name'] ?></span><br/>
                                <span class="card-text">Code: <?= $bank['code'] ?></span><br/>
                                <span class="card-text">Payment type: <?= $bank['payment_type'] ?></span>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</main>