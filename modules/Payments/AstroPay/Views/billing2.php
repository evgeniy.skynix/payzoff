<div class="container">
    <?php if ($error): ?>
        <div class="row">
            <span  style="color: #990000" id="error" param="<?=$error['param']?>">Error: <?= $error['description']  ?></span>
        </div>
        <pre>
        <?php extract($post) ?>
    <?php endif; ?>
            </pre>
    <div class="row">

        <div class="col-md-4 order-md-2 mb-4">
            <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Your cart</span>
                <span class="badge badge-secondary badge-pill">3</span>
            </h4>
            <ul class="list-group mb-3">
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Product name</h6>
                        <small class="text-muted">Brief description</small>
                    </div>
                    <span class="text-muted">$12</span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Second product</h6>
                        <small class="text-muted">Brief description</small>
                    </div>
                    <span class="text-muted">$8</span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Third item</h6>
                        <small class="text-muted">Brief description</small>
                    </div>
                    <span class="text-muted">$5</span>
                </li>
                <li class="list-group-item d-flex justify-content-between bg-light">
                    <div class="text-success">
                        <h6 class="my-0">Promo code</h6>
                    </div>
                    <span class="text-success">-$5</span>
                </li>
                <li class="list-group-item d-flex justify-content-between">
                    <span>Total (USD)</span>
                    <strong>$20</strong>
                </li>
            </ul>

        </div>
        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Billing</h4>
            <form class="needs-validation" novalidate action="/billing/2" method="POST">
                <input type="hidden" name="amount" value="<?= $amount ?? 20 ?>">
                <div class="mb-3">
                    <label for="username">Username</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text">@</span>
                        </div>
                        <input type="text" class="form-control" id="username" name="username" value="<?= $username ?? null ?>" placeholder="Username"
                               required
                        >
                        <div class="invalid-feedback" style="width: 100%;">
                            Your username is required.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">First name </label>
                        <input type="text" class="form-control" id="firstName" name="firstName" value="<?= $firstName ?? null ?>" placeholder=""
                               required
                        >
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Last name </label>
                        <input type="text" class="form-control" id="lastName" name="lastName" value="<?= $lastName ?? null ?>" placeholder=""
                               required>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="x_cpf">Personal identification number </label>
                    <input type="text" class="form-control" id="x_cpf" placeholder="123456789" value="<?= $x_cpf ?? null ?>"
                           name="x_cpf"
                           required
                    >
                    <div class="invalid-feedback" >
                        Please enter a valid personal identification number.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="email">Email </label>
                    <input type="email" class="form-control" id="email" placeholder="you@example.com" value="<?= $email ?? null ?>"
                           name="email"
                           required
                    >
                    <div class="invalid-feedback">
                        Please enter a valid email address for shipping updates.
                    </div>
                </div>
                <div class=" mb-3">
                    <label for="country">Country</label>
                    <select class="custom-select d-block w-100" id="country" name="country" required>
                        <option value="">Choose...</option>
                        <?php foreach ($countries as $code => $name): ?>
                            <?php $selected = isset($country) && $country == $code ? 'selected' : null ?>
                            <option <?=$selected?>  value="<?= $code ?>"><?= $name ?></option>
                        <?php endforeach; ?>
                    </select>
                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                </div>
                <div class="mb-3">
                    <label for="city">City <span class="text-muted">(Optional)</span></label>
                    <input type="text" class="form-control"
                           id="city" placeholder="Santa Isabel"
                           name="city"
                           value="<?= $city ?? null ?>"
                    >
                </div>
                <div class="mb-3">
                    <label for="address">Address<span class="text-muted">(Optional)</span></label>
                    <input type="text" class="form-control"
                           id="address" placeholder="1234 Main St"
                           name="address"
                           value="<?= $address ?? null ?>"
                    >
                </div>
                <div class="mb-3">
                    <label for="phone">Phone<span class="text-muted">(Optional)</span></label>
                    <input type="text" class="form-control"
                           id="phone" placeholder="11987659876"
                           name="phone"
                           value="<?= $phone ?? null ?>"
                    >
                </div>

                <hr class="mb-4">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="same-address" name="same-address">
                    <label class="custom-control-label" for="same-address">Shipping address is the same as my billing
                        address</label>
                </div>
                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
            </form>
        </div>
    </div>
</div>
