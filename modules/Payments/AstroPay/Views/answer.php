<div class="container">
    <div>
        <?php if ($error): ?>
            <div class="row">
                <span  style="color: #990000">Error: <?= $error ?></span>
            </div>
        <?php endif; ?>
        <form class="needs-validation" novalidate action="/paystatus" method="POST">
            <div class="row">
                <div class="col-2 ">
                    Invoice Id:
                </div>
                <div class="col-8">
                    <input type="text" class="form-control wy-text-small" id="invoiceID"
                           value="<?= $invoice ?>"
                           name="invoiceID"
                           placeholder="Doc32131309"
                           required
                    >
                    <div class="invalid-feedback">
                        Valid first name is required.
                    </div>
                </div>
                <div class="col-2">
                    <button class="btn btn-primary btn-small btn-block" type="submit">Check</button>
                </div>
            </div>

        </form>
    </div>
    <?php foreach ($rows as $key => $val): ?>
        <?php if (!$val) continue; ?>
        <div class="row">
            <div class="col-4"><?= $key ?></div>
            <div class="col-8"><?= $val ?></div>
        </div>
    <?php endforeach; ?>
</div>
