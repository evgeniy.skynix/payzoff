<?php


namespace Payments;


interface PaymentsInterface
{

    /**
     * Get answer from payment system
     *
     * @return mixed
     */
    public function answer(): string;

    /**
     * @return string
     */
    public function confirm(): string;

    /**
     * @return string
     */
    public function refund(): string;

    /**
     *
     * @param null $invoice
     * @return mixed
     */
    public function getPayStatus($invoice = null): string;

}