<?php

namespace Payments;

abstract class PaymentsBase
{
    private static $errors = 0;

    protected static function curl($url, $paramsArray)
    {
        $params = array();
        foreach ($paramsArray as $key => $value) {
            $params[] = "{$key}={$value}";
        }
        $params = join('&', $params);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $response = curl_exec($ch);
        if (($error = curl_error($ch)) != false) {
            self::$errors++;

            if (self::$errors >= 5) {
                die("Error in curl: $error");
            }

            sleep(1);
            self::curl($url, $paramsArray);
        }
        curl_close($ch);

        self::$errors = 0;
        return $response;
    }
}